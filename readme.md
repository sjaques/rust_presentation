# Rust presentations

## Versions

- See rust_presentation_Xmin/hour.md


## Preparation

1. Test a dummy Teams meeting with
   1. Sound
   2. Content sharing (full screen)
   3. Camera (with face on content)
2. Install VSCode & plugins:
3. rust-analyzer
4. vscode-pdf
5. marpit


## Just before presentation

1. When using WSL: set DISPLAY
2. Set screen resolution to 1920x1080 or 1680x1050
3. Open VSCode on this repo
4. Open PDF with presentation on Page Fit view
5. Open browser without clutter
6. Open rust cheat sheet


## Tools

Running Rust graphics demo:

```sh
cd ~/repos/upstream/Iced
cargo run --features iced/glow--package solar_system
```

Run on Windows & WSL


## To do list

- [x] `if letSome(i) = num` is confusing and can be left out
- [x] lot of contents, need to remove/split things -> short & long version
- [x] gets complex very fast, but more the "get used to"
- [x] check page numbering & header
- [x] lifetime annotations are not the most crucial feature,
      just mention compiler cannot always figuring out lifetimes
