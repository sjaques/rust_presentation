---
marp: true
title: Rust
theme: default
style: |
  section {
    display: block;
    place-content: safe center center;
    font-size: 35px;
    height: 720px;
    letter-spacing: 2px;
    line-height: 1.4;
    padding: 50px 120px;
    width: 1280px;
    word-wrap: break-word;
  }

  :is(h1, h2, h3) {
    font-variant: small-caps;
    text-align: center;
    color: black;
  }
  :is(h4, h5, h6) {
    text-align: center;
  }
  code {
    font-family: SFMono-Regular, Consolas, 'Liberation Mono', Menlo, Courier,
      monospace;
    letter-spacing: 0;
  }
  & > code,
  *:not(pre) > code {
    background: var(--color-background-code);
    color: var(--color-foreground);
    margin: -0.2em 0.2em 0.2em;
    padding: 0.2em;
  }
  img {
    background: transparent;
  }

mark_down_annotations:
---

![bg left](./images/rust_background.png)
<style scoped>
    section {
        font-size: 30px;
        text-align: center;
    }
</style>

# Rust
www.rust-lang.org
<br>
<br>

*by Sam Jaques*

![width:100](./images/me_circle.png)
![width:120](./images/tmc_logo_small.png)

___
<!-- paginate: true -->
<!-- header: "rust" -->

# For who?

Mainly for C/C++ developers
- Rust basics
- Similar features: informal
- The unknowns: deep dive

___

# Contents

- Rust Pillars
- Media
- C/C++ Comparisons
- Basics
- Advanced
- Tooling
- Conclusion

___

# Rust, what?
<style scoped> section { text-align: center; }</style>

![width:800](./drawings/rust_what.drawio.svg)

[All Rust editions:link:](https://doc.rust-lang.org/edition-guide/index.html)
[LLVM:link:](https://en.wikipedia.org/wiki/LLVM)
___
<style scoped> section { text-align: center; }</style>

![bg](./images/rusty_background.png)

# Rust Pillars
<br>

![width:800](./drawings/rust_pillars.drawio.svg)

___

# :one: Reliability

## :star: Memory safe (by default)
- No invalid indexes
- No dangling pointers
- No memory corruptions
- No resource leaks,
- No overflows, ...

**guaranteed** @ compile time!
:arrow_right: No Valgrind, no memory testing

___
<style scoped> section { text-align: center; }</style>

# :two: Performance

:bullettrain_side: Blazingly fast  <!-- C & C++ alike -->
:pinching_hand: memory-efficient
:deciduous_tree: no garbage collector

Benchmarks:
[Rust vs C++](https://benchmarksgame-team.pages.debian.net/benchmarksgame/fastest/rust-gpp.html), [Rust vs C](https://benchmarksgame-team.pages.debian.net/benchmarksgame/fastest/rust.html)
<!-- squeezed out all performance features -->

___
<style scoped> section { text-align: center; }</style>

# :three: Productivity
![width:800](./drawings/productivity.drawio.svg)
1 command install:
```sh
curl --proto '=https' --tlsv1.2 -sSf https://sh.rustup.rs | sh
```
___
<!-- paginate: true -->


# Media
![bg](./images/rusty_background.png)

___


[Why Discord switched from Go to Rust](https://blog.discord.com/why-discord-is-switching-from-go-to-rust-a190bbca2b1f)

> *Even with just basic optimization, Rust was able to outperform the hyper hand-tuned Go version. This is a huge testament to how easy it is to write efficient programs with Rust compared to the deep dive we had to do with Go.*

![width:800](https://miro.medium.com/max/3000/1*-q1B4t622mnxoV8kvT9RwA.png) <span style="color:#449acb;font-size:50%">● Rust</span> <span style="color:#604b8d;font-size:50%">● Go</span>

___



[Linus Torvalds response in IT Wire](https://itwire.com/open-source/rust-support-in-linux-may-be-possible-by-5-14-release-torvalds.html):

> *Rust support was 'not there yet', adding that things were 'getting to the point where maybe it might be mergeable for 5.14 or something like that...'*

[Linux 6.1 Officially adds support for Rust in the Kernel ](https://www.infoq.com/news/2022/12/linux-6-1-rust/)
> *Unless something odd happens, it (Rust) will make it into 6.1.*

[➡️ It made it!](https://lore.kernel.org/lkml/202210010816.1317F2C@keescook/)

___

[Google is now writing low-level Android code in Rust](https://arstechnica.com/gadgets/2021/04/google-is-now-writing-low-level-android-code-in-rust/)

> *memory safety bugs continue to be a top contributor of stability issues, and consistently represent ~70% of Android’s high severity security vulnerabilities.*

> *Most of our memory bugs occur in new or recently modified code. Rust will be used for new components, when necessary, which should help reduce any new memory bugs Google could introduce.*

___



[AWS: Innovating with Rust](https://aws.amazon.com/blogs/opensource/innovating-with-rust/)

> *"The Rust language has quickly become critical to building infrastructure at scale at Amazon Web Services (AWS)"*

[AWS: Sustainability with Rust](https://aws.amazon.com/blogs/opensource/sustainability-with-rust/)

> *"We started hiring Rust maintainers and contributors, partnered with Google, Huawei, Microsoft and Mozilla to create the Rust Foundation with a mission to support Rust. AWS is investing in the sustainability of Rust, a language we believe should be used to build sustainable and secure solutions. "*

___

[Most loved language since 2015 YoY](https://survey.stackoverflow.co/2024/technology#admired-and-desired)

> *Rust continues to be the most-admired programming language with an 82% score in 2024.*

<span style="color:#0095ff;font-size:50%">● Desired</span> <span style="color:#ff4453;font-size:50%">● Admired</span>
![most_beloved width:1000](./images/stack-overflow-most-loved-programming-languages.png)

___

# Downsides

- Steep learning curve <!-- concepts from system programming like C, with higher level language like Scala & Haskell -->
- Complexity
  - multiple _pointer_ types (`&`,`Rc`,`Arc`,`Box`,`Cell`...)
  - two different string types (`&str`and`std::String`)
  - generics
  - lifetimes can be confusing
- Young & small (but mature) community

___

<style scoped> section { text-align: center; }</style>
![bg](images/rusty_background.png)

![width:800](images/rust_vs_c++.jpg)

# C/C++ Comparisons

___


# Advantages over C/C++?

- Memory & thread safety
- Forced conventions <!-- less discussions, dir structure, static typing, borrowing rules, formatter etc -->
- More compile time checking
- Shorter & readable compiler errors
- Safe arithmetics
- No header files, copy/move c'tors :arrow_right: Less boilerplate
- C++ drags a lot of legacy :arrow_right: complexity goes :arrow_up:
- No need for a build system :arrow_right: `cargo`:truck: does it all!<!-- definitions within main config file -->
- ...
___
<style scoped> section { text-align: center; }</style>

# Development time

Time spend on learning & bug fixing

![width:1000](./drawings/time_spend.drawio.svg)

_(estimation, no hard science)_
<!-- A tool makes you a fool?, think again! -->
___

# Semantic similarities

- Types, functions & structs
- Similar to modern C++ :arrow_right: easier to learn
- moves are forced (implicitly)
- immutable by default
- `&`are safer & clearer (explicit): `hold_my_vec(&v)` <!-- in C++ no `&` symbol is present when passing by reference -->
- `&self`⇔`this: &Self`⇔ C++`this`pointer

___


# Other known features
- **Macros**: conceptionally the same, harder to misuse
  _examples_: `vec!`,`println!`,`panic!`, `#[cfg(test)]`

- **Aliases**: `use u32 as number` (like `using`)
- **Generics`<T>`**: similar to templates, used more often
- **Closures**: similar to lambdas (`Fn`,`FnMut`, `FnOnce`) _examples_:
  ```rust
    fn  add_one_v1   (x: u32) -> u32 { x + 1 }
    let add_one_v2 = |x: u32| -> u32 { x + 1 };
    let add_one_v3 = |x: u32|        { x + 1 };
    let add_one_v4 = |x: u32|          x + 1  ;
  ```

___


# C++ Compile error hints:weary:

    ‘from std::_Rb_tree<_Key, _Val, _KeyOfValue, _Compare, _Alloc>::iterator
    std::_Rb_tee<_Key, _Val, _KeyOfValue, _Compare, _Alloc>::
    _M_emplace_hint_unique(std::_Rb_tree<_Key, _Val, _KeyOfValue, _Compare,
    _Alloc>::const_iterator, _Args&& ...) [with _Args =
    {const std::piecewise_construct_t&, std::tuple<const SomeType&>,
    std::tuple<>}; _Key = SomeType; _Val = std::pair<const SomeType, SomeData>;
    _KeyOfValue = std::_Select1st<std::pair<const SomeType, SomeData> >;
    _Compare = std::less<SomeType>; _Alloc =
    std::allocator<std::pair<const SomeType, SomeData> >;
    std::_Rb_tree<_Key, _Val, _KeyOfValue, _Compare, _Alloc>::iterator =
    std::_Rb_tree<SomeType, std::pair<const SomeType, SomeData>,
    std::_Select1st<std::pair<const SomeType, SomeData> >,std::less<SomeType>,
    std::allocator<std::pair<const SomeType, SomeData> > >::iterator;
    std::_Rb_tree<_Key, _Val, _KeyOfValue, _Compare, _Alloc>::const_iterator =
    std::_Rb_tree<SomeType, std::pair<const SomeType, SomeData>,
    std::_Select1st<std::pair<const SomeType, SomeData> >, std::less<SomeType>,
    std::allocator<std::pair<const SomeType, SomeData> > >::const_iterator]’

<!-- Only 2 types created: `SomeType` & `SomeData` rest is std -->

___

# Rust compiler error hints:smile:

![rust compiler hints](images/rust_compiler_hints.png)

___

# Unsafe arithmetics [:link:](https://godbolt.org/z/9b9P6KGfx)

```cpp
// C++
auto square(std::int32_t num) {
    return num * num;
}
```
:arrow_right: the range depends on the arithmetical operation

**Range: `[-46340, 46340]` == `[0xFFFF4AFC, 0xB504]` :-1:**

`46341 * 46341`:arrow_right: UB: overflow :exclamation:
`-46341 * -46341`:arrow_right: ditto
___

# Safe arithmetics [:link:](https://rust.godbolt.org/z/GhenxYbcb)

```rust
// rust
fn square(num: i32) -> i32 {
    num * num
}
```
:arrow_right: same range

UB? :arrow_right: literally impossible but panic:
> *"attempt to multiply with overflow"*

Handle with:
- `num.checked_mul(num)`[:link:](https://doc.rust-lang.org/std/primitive.i32.html#method.checked_mul)
- `num as u64 * num as u64`
___


![bg](./images/rusty_background.png)

# Basics

___


- Compilation
- Variables & Constants
- Mutability
- Control flow
- Primitive types
- Vectors
- Rust's "secret sauce"
- Structs
- Enums
- Pattern matching
- Strings & Slices

___

# Compilation

- 1 compiler: `rustc`
- Supported platforms [:link:](https://doc.rust-lang.org/stable/rustc/platform-support.html)
- Cross platform by default

    ```sh
    ❯ rustc src/main.rs --target=aarch64-wrs-vxworks
    ```

- Used by `cargo`
- E.g.: running an example
- `examples/NAME.rs`
  ```sh
  ❯ cargo run --example <NAME>
  ```
- [Setup exercises here:link:](https://github.com/jaques-sam/rust_course_material#exercises)

___
<style scoped> section { text-align: center; }</style>

# Memory layout

![width:500](drawings/memory_layout.drawio.svg)

___

# Variables & Constants

```rust
// Globals: declared outside all scopes
static LANGUAGE: &str = "Rust";  // `'static` lifetime
const THRESHOLD: i32 = 10;

fn main() {
    const N: i32 = 16i32; // optional type suffix

    println!("This is {LANGUAGE}");
    println!("The threshold is {THRESHOLD}");
    let saturated = N > THRESHOLD;

    THRESHOLD = 5;  // Error! Cannot modify a `const`.
    LANGUAGE = "C++"; // Error!
}
```
___

# Mutability

- Variables are immutable (= unchangeable) by default
- Add `mut` before variable name

```rust
fn increment(n: &mut u32) {
    *n += 1;
}

fn main() {
    let mut x = 1;
    increment(&mut x);
    println!("{x}");
}
```
___


# Control flow

- `if`/`else` including:
  ```rust
  let number = if condition { 5 } else { 6 };
  ```
- `loop {...}` ⇔ `while true {...}`  <!-- including break & continue -->
- ranged loops:
  ```rust
  for number in (1..4).rev() {
      println!("{}!", number);
  }
  ```
- loop labels:
  ```rust
  let magic = 'a: loop { ... break 'a 42; ... }`
  ```

___

# Primitive types [:link:](https://doc.rust-lang.org/std/index.html#primitives)

- `bool`,`char`,`f32`,`f64`
- `i8`,`u8`,`i16`,`u16`,... ,`i128`
- `usize`,`isize` (hardware dependent)
- fixed-size array `[T; N]`[:link:](https://doc.rust-lang.org/std/primitive.array.html)
- function pointer `fn`
- reference`&` and `mut&` (and unsafe pointer`*mut T`)
- slice `[T]` and string slice `str`
- tuple`(T, U, ..)`
___

# Vectors

- One of must used types
- Values on the heap

Initialization:
```rust
let mut v1: Vec<i32> = Vec::new();  // optional type forcing
let v2 = vec![1, 2, 3];  // type i32 inferred
```
Updating/reading:
```rust
v1.push(1);
let _third: &i32 = &v2[2]; // can panic, use .get() instead
v1.iter_mut().for_each(|e| *e += 1);
let _v3: Vec<i32> = v1.iter().map(|e| e + 1).collect();
```
___

<style scoped> section { text-align: center; }</style>

# Rust's "secret sauce"

**Enforced Rules
\+
The Borrowing Rules**

___

<style scoped> section { text-align: center; }</style>

# Enforced Rules

**If you allow something, people use it**

_([In C++ 18 ways of initializing a variable :open_mouth:](20_ways_to_initialize_a_variable_in_cpp.md))_
<!-- this doesn't mean they are all good. They can lead to inconsistency too. -->

<br>

**If you don't enforce, people make mistakes**
_(even the best :horse:)_

___
<style scoped> section { text-align: center; }</style>

# The borrow checks [:link:](https://rustc-dev-guide.rust-lang.org/borrow_check.html)

**Many immutable borrows as you like (`&`)
**OR**
A single mutable borrow  (`&mut`)**

___

# Transfer of ownership

- Inexpensive by design
- Implicitely enforced

<br>

![width:600](./drawings/ownership.drawio.svg)

<!-- includes forced *move semantics" -->

___

Borrowing example

```rust
fn hold_my_vec<T>(_: Vec<T>) {
}

fn main() {
    let v = vec![1, 2, 3, 5, 7, 11, 13, 17];
    hold_my_vec(v);  // culprit
    let element = v.get(3);

    println!("Element '{:?}' taken from vector", element);
}
```
___
Compiler prints understandable error

```rust
error[E0382]: borrow of moved value: `v`
--> src/main.rs:6:19
    |
  4 | let v = vec![2, 3, 5, 7, 11, 13, 17];
    |     - move occurs because `v` has type `std::vec::Vec<i32>`,
    |       which does not implement the `Copy` trait
  5 | hold_my_vec(v);
    |             - value moved here
  6 | let element = v.get(3);
    |               ^ value borrowed here after move
```

___

Borrow the vector as **immutable** reference

```rust
fn hold_my_vec<T>(_: &Vec<T>) {}

fn main() {
    let v = vec![2, 3, 5, 7, 11, 13, 17];
    hold_my_vec(&v);
    let element = v.get(3);

    println!("I got this element from the vector: {:?}", element);
}
```

___

# Lifetime constraints

```rust
{
    let x: &Vec<i32>;
    {
        let y = Vec::new();

        x = &y;

    }
    println!("x's length is {}", x.len());
}

error: `y` does not live long enough
```
___

# Lifetime constraints

```rust
{
    let x: &Vec<i32>;
    {
        let y = Vec::new();  //--+
//                               | y's lifetime
//                               |
        x = &y;  //--------------|--------------+
//                               |              |
    }  // <----------------------+              | x's lifetime
    println!("x's length is {}", x.len());//    |
}  // <-----------------------------------------+

error: `y` does not live long enough
```

___

# Structs

```rust
struct User {  // Definition
    username: String,
    active: bool,
}

impl User {  // Implementation
    fn isActive(&self) -> bool { self.active }
}

struct ColorRGB(i32, i32, i32);  // Tuple struct
struct UnitStruct;

fn main() {
    let sj = User {  // Instantiation
        username: String::from("SJ"),
        active: true,
    };
    let black = ColorRGB(0, 0, 0);
    let isActive = sj.isActive();
}
```

___

# Init struct from template

```rust
impl User {
    fn create_from_template() -> User {
        User {
            username: "unknown".to_string(),
            active: true,
        }
    }
}

fn main() {
    let bk = User {
        username: String::from("BK"),
        ..User::create_from_template()
    };
}
```

___


# Enums

```rust
enum Message {
    Quit,  // first variant
    Move{x: i32, y: i32},  // struct
    Write(String),  // tuple struct
    ChangeColor(i32, i32, i32),  // tuple struct
}  // biggest variant size => Message size

impl Message {
    fn call(&self) {
        // tadaaa
    }
}

let m = Message::Write(String::from("hello"));
m.call();
```

___

# Pattern matching

```rust
let optional = Some(0);

match optional {
    Some(i) => println!("{i}"),
    None => println!("No value.")
}
```

___

# Pattern matching

<!-- Match expression is exhaustive: need all patterns! -->

```rust
match number {
    1 => println!("one"),
    2 | 3 => println!("two or three"),
    4..=9 => println!("within range"),
    x if x > 10 => println!("more than ten: {x}"),
    x => println!("{x}"),  // OR next line
    _ => println!("default Case")  // -> exhaustive
}
```
```rust
let version = SemVer(1, 32, 2);
match version {
    SemVer(major, _, _) => {
        println!("{major}");
    }
}
```

___

# FLow control<br>`if let` / `while let`

  ```rust
  match number {
    Some(i) => println!("number is: {i}"),
    _ => {},
  };
  ```
  vs.
  ```rust
  if let Some(i) = number {  // i = number.1 ; shorter match
    println!("number is: {i}");
  }
  ```
Same for `while let`: [example :link:](https://doc.rust-lang.org/rust-by-example/flow_control/while_let.html)

___

# Strings & Slice type `&[T]`

``` rust
let s = "Hello world".to_owned(); // or .to_string()
let hello: &str = &s[0..5];
let world: &str = &s[6..11];
```
```rust
fn first_word(s: &String) -> &str {
    let bytes = s.as_bytes();
    for (i, &item) in bytes.iter().enumerate() {
        if item == b' ' {
            return &s[0..i];
        }
    }

    &s[..]
}
```

___

# Slice type's ["fat pointer:link:"](https://stackoverflow.com/a/57754902/2522849)
<style scoped> section { text-align: center; }</style>

![width:500](./drawings/string_slice.drawio.svg)

___

![bg](./images/rusty_background.png)

# Advanced topics

___

- Modules & file structure
- Crates & Visibility
- Traits
- Error handling
- Smart pointers & containers
- Pointer types
- Iterations
- Fearless Concurrency

___


# Modules

- Hierarchical
- Paths from inside are private by default
- Root of crate is called`crate` (optional)

```rust
mod house { // extract to file with rust-mod-generator
    pub mod kitchen {
        pub fn turn_on_light() {}
    }
}

pub fn turn_on_all_lights() {
    crate::house::kitchen::turn_on_light();
}
```
___

# Module file structure

Modules can be directory structured

There are 2 ways:

1. Code in *./module_name/mod.rs*
2. Code in *./module_name.rs* (since 2018)

___

# Module file structure

### Code in *./module_name/mod.rs*

```
❯ tree home
🗁 src
├── lib.rs  → home module code
└──🗁 house
    ├── mod.rs  → house module code
    ├──🗁 kitchen
    │   └── mod.rs  → kitchen module code
    ├──🗁 livingroom
    │   └── mod.rs
...
```

___

# Module file structure

### Code in *module_name.rs*
### with optional *./module_name* dir

```
❯ tree home
🗁 src
├── lib.rs  → home module code
├── house.rs  → house module code
└──🗁 house
    ├── kitchen.rs  → kitchen module code
    ├──🗁 kitchen
    ├── livingroom.rs  → optional dir
...
```


___

# Crates & Visibility

![width:1000](./drawings/visibility_pub.drawio.svg)

___

# Crates & Visibility

![width:1000](./drawings/visibility_pub_use.drawio.svg)

___

## Options over null values
Tony Hoare (inventor of _null_):

  > _Null References: The Billion Dollar Mistake_

<br>

:arrow_right: Value _present_ or _absent_:
<!-- try to use a null value as a not-null value, you’ll get an error of some kind -->

```rust
enum Option<T> {
    None,
    Some(T),
}
```
___

## Extensive Option API

```rust
orig_errors.is_empty().then_some(Valid).unwrap_or(Invalid {
    errors: orig_errors.iter().map(Into::into).collect(),
})
```
<!-- previous will give a clippy warning -->
vs.

```rust
if orig_errors.is_empty() {
    Valid
} else {
    Invalid {
        errors: orig_errors.iter().map(Into::into).collect(),
    }
}
```
___

# Error handling

1. Recoverable errors: `Result<T, E>` and `?`
2. Unrecoverable errors: `panic!`like C`assert`

No concept of "exceptions"

___


## Error handling in ⩾C++1z

```cpp
#include <string>
#include <optional>
#include <fstream>

std::optional<std::string> read_line_from_file() {
    std::ifstream file("my_file.txt");
    if (!file) {
        return {};
    }

    std::string line;
    file >> line;
    if (!file.good()) {
        return {};
    }
    return line;
}
```

___

## Error handling in Rust (long)

```rust
use std::fs::File;
use std::io::{self, Read};

fn read_line_from_file() -> Result<String, io::Error> {
    let f = File::open("my_file.txt");
    let mut f = match f {
        Ok(file) => file,
        Err(e) => return Err(e),
    };

    let mut s = String::new();
    match f.read_to_string(&mut s) {
        Ok(_) => Ok(s),
        Err(e) => Err(e),
    }
}
```

___


## Error handling in Rust (short)

```rust
use std::fs::File;
use std::io::{self, Read};

fn read_line_from_file() -> Result<String, io::Error> {
    let mut file = File::open("my_file.txt")?;  //--> propagate error
    let mut line = String::new();
    file.read_to_string(&mut line)?;
    Ok(line)
}
```
Or simply :smile:
```rust
let content = fs::read_to_string("my_file.txt")?;
```

___

<!-- paginate: false -->

# Traits
⇔ Group of interfaces

- :heavy_check_mark: **Dynamic dispatch** :arrow_right: trait objects (`dyn`)
:x: inheritance
- :heavy_check_mark: **Static dispatch** :arrow_right: with generics

```rust
trait Animal {
    fn noise(&self) -> String { String::from("") }
    fn has_tail(&self) -> bool;
}
```

___

# Traits
## Implemented on struct

```rust
struct Dog { name: String }

impl Animal for Dog {
    fn noise(&self) -> String { String::from("woof!") }
    fn has_tail(&self) -> bool { true }
}

impl Dog {
    fn new(name: &str) -> Self {
        Dog { String::from(name) }
    }
    fn fetch(&self) { }
}
```
___
<!-- paginate: true -->

# Traits
## Dynamic dispatch

```rust
struct Zoo {
    animals: Vec<Box<dyn Animal>>, // fat pointer
}

impl Zoo {
    fn make_some_noise(&self) {
        for animal in self.animals.iter() {
            println!("{}", animal.noise());
        }
    }
}

```

___

# Traits
## Example: **Deref**

Own smart pointer

```rust
struct MyBox<T>(T);  // tuple struct

impl<T> MyBox<T> {
    fn new(x: T) -> MyBox<T> {
        MyBox(x)
    }
}
```

___


## Example: **Deref**

```rust
use std::ops::Deref;

impl<T> Deref for MyBox<T> {
    type Target = T;

    fn deref(&self) -> &T {
        self.0
    }
}

fn main() {
  let x = 5;
  let y = MyBox::new(x);
  assert_eq!(5, *y);
}
```
___


# Traits
## Deref coercion (= call chain)

```rust
fn hello(name: &str) {
    print!("Hello {}!", name);
}

fn main() {
    let name = Box::new(String::from("Sam"));
    hello(&name);  // &Box<String> -> &String -> &str

    hello(&(*name)[..]);  // without Deref coercion
}
```
___

# Lifetime annotations

- Compiler cannot always determine lifetimes
- Getting better each release
- Following example won't compile:
```rust
fn longest(x: &str, y: &str) -> &str {
    if x.len() > y.len() {
        x
    } else {
        y
    }
}
```

___

# Iterations
## In C++
```cpp
v.erase(std::remove_if(std::begin(v), std::end(v), is_odd), std::end(v));
```
:arrow_right: contains 2 loops

## In Rust
```rust
let v: Vec<_> = v.iter().filter(|&x| !is_odd(x)).collect();
```
:arrow_right: `collect`consumes all`iter`s, once
<!-- `collect` can also convert a container to another type-->

___


## Iterator performance

- Zero-cost abstractions: no runtime overhead
- Compiles identical to assembly without loop
- Loop vs iterator:
    1. The Adventures of Sherlock Holmes :arrow_right: string
    2. Count the word _the_
```
test bench_search_for  ... bench:  19,620,300 ns/iter (+/- 915,700)
test bench_search_iter ... bench:  19,234,900 ns/iter (+/- 657,200)
```
___

# std types in prelude [:link:](https://doc.rust-lang.org/std/prelude/index.html)
<!-- prelude: list of things that Rust automatically imports into every Rust program.
 It’s kept as small as possible, focused particularly traits, used in almost every program.-->

| Rust STL              | C++ STL equivalent |
| :-------------------- | :----------------- |
| `std::boxed::Box`     | `std::unique_ptr`  |
| `std::vec::Vec`       | `std::vector`      |
| `std::option::Option` | `std::optional`    |
| `std::result::Result` | -                  |
| `std::string::String` | `std::string`      |
...

<!-- Much more in prelude like
- std::marker::{Copy, Send, Sized, Sync, Unpin}
- std::ops::{Drop, Fn, FnMut, FnOnce}
- std::clone::Clone
- std::cmp::{PartialEq, PartialOrd, Eq, Ord}
-->
___

# smart pointers & containers

- `std::boxed::Box<T>`
- `std::rc::Rc<T>` (=<ins>**R**</ins>eference <ins>**c**</ins>ounting)
- `std::sync::Arc<T>`(=<ins>**A**</ins>tomic)
- `std::cell::Cell<T>|RefCell<T>`
- `std::collections::HashMap<T,U>|HashSet<T>`
- `std::collections::LinkedList`

C++`std::shared_ptr` equivalent?:`Arc<Mutex<T>>` or `Arc<RwLock<T>>` or `Arc<UnsafeCell<T>>`

<!--  Rc<T> is a safe default, because the compiler will catch any attempt to send an Rc<T> between threads-->

___

| Pointer type | Features [:link:](https://manishearth.github.io/blog/2015/05/27/wrapper-types-in-rust-choosing-your-guarantees/) |
| :----------- | :--------------------------------------------------------------------------------------------------------------- |
| `&T`         | Single ownership                                                                                                 |
| `&mut T`     | Single ownership, interior mutability                                                                            |
| `Box<T>`     | Single ownership                                                                                                 |
| `Rc<T>`      | Multiple ownership                                                                                               |
| `Arc<T>`     | Multiple ownership, threadsafe                                                                                   |
| `Cell<T>`    | Interior mutability for`copy`types                                                                               |
| `RefCell<T>` | Interior mutability                                                                                              |
| `Mutex<T>`   | Interior mutability, threadsafe                                                                                  |
| `RwLock<T>`  | Same as mutex, 1 lock for readers                                                                                |
<!-- Interior mutability: In other words, they contain data which can be manipulated even if the type cannot be obtained in a mutable form -->

___

# Heap allocated data

<table align="center">
<td>
<img src="drawings/heap_memory_usage.drawio.svg" width=280>
</td>

<td>
</td>

<td>

```rust
let main() {
    let result = heap();
}

fn heap() -> Box<u32> {
    let b = Box::new(88);
    b
}
```
[:link: Sreekanth video](https://www.youtube.com/watch?v=7_o-YRxf_cc)
</td>
</table>

___

<style scoped> section { text-align: center; }</style>

# Pointer types

![width:800](./drawings/pointer_types_more.drawio.svg)

___

## A (Ref)Cell?

![width:500](./drawings/cell_within_immutable_struct.drawio.svg)

- :exclamation: Borrowing checks @ runtime!
**Hazard:** exclusive access needed when modifying

- `Cell<Rc<T>>`&`Cell<Arc<T>>` :arrow_right: mutable shared ownership

___

#### `Cell<T>` vs `mut &T`

<table>
<tr>
<th> Runtime check </th>
<th> Compile check </th>
</tr>
<tr>
<td>

```rust
    let x = Cell::new(1);
    let y = &x;
    let z = &x;
    x.set(2);
    y.set(3);
    z.set(4);
    println!("{}", *x);
```

</td>
<td>

```rust
    let mut x = 1;
    let y = &mut x;
    let z = &mut x;
    x = 2;
    *y = 3;
    *z = 4;
    println!("{}", x);
```

</td>
</tr>
</table>

- Same behavior
- Same runtime cost: none!:thumbsup:

___

![bg](./images/rusty_background.png)
# Fearless Concurrency

___

# Message passing
#### _[preferred]_

> Do not communicate by sharing memory;
> instead, share memory by communicating.
<br>
Bowering rules :arrow_right: only 1 owner :heart:

___

# Message passing: example

```rust
use std::sync::mpsc;
use std::thread;

fn main() {
    let (tx, rx) = mpsc::channel();

    thread::spawn(move || {  // tx moves to closure
        let val = String::from("hi");
        tx.send(val).unwrap();
    });

    let received = rx.recv().unwrap();
    println!("Got: {}", received);
}
```

___

# Shared state

- Immutuable data :arrow_right:`Arc<RefCell<T>>`
- Mutable data :arrow_right: `RwLock<T>`or`std::Mutex<T>`

<br>

Locking dicipline enforced :heart::

**lock-protected data is only accessible
when holding the lock**

___

## Dead locks still possible :anguished: ...
_when using a `Mutex`/`RwLock`_

```rust
use std::sync::Mutex;

fn main() {
    let data = Mutex::new(0);
    let _d1 = data.lock();
    let _d2 = data.lock(); // cannot lock, since _d1 is still active

    println!("is not printed");
}
```
___

## ... not necessarily :smile:

```rust
use cooptex::*;

fn main() {
    let a = CoopMutex::new(42);  // wrapped Mutex

    retry_loop(|| {
        let first = a.lock()?.unwrap();
        let second = a.lock()?.unwrap(); // will panic

        assert_eq!(*first + *second, 84);
        Ok(())
    });
}
```

___

# Common Dead Lock

```diff
+   let (lights, light) = {
    let inner = self.inner.lock().unwrap();
-   inner.lights.dim(&light)  // lock still held here
+       (inner.lights.clone(), inner.light.clone())
+   };
+
+   lights.dim(&light)  // don't lock while calling this
```
___

# A word about async/await

- STD doesn't come with an async runtime
- Most mature runtime: [:link:tokio.rs](https://tokio.rs/)
- Syntactic sugar for using & handling Futures
- There is a separate [:link:async book](https://rust-lang.github.io/async-book/)

```rs
async fn learn_and_sing() {
    let song = learn_song().await;
    sing_song(song).await;
}
async fn async_main() {
    let f1 = learn_and_sing();  // while blocked -> dance
    let f2 = dance();  // while blocked -> learn and sing
    futures::join!(f1, f2); // waits for multiple futures concurrently
    // If both futures are blocked, then `async_main` is blocked
```
___

# Unit test example with Tokio runtime

```rust
    #[tokio::test]
    async fn starting_streamer_that_does_not_exist_should_fail() {
        let manager = Streamers();

        assert!(manager.start("id").await.is_err());
    }
```

___

# Async traits

- Rust supports async in (private) traits
- Need a async pub trait: [:link:`async_trait`](https://docs.rs/async-trait)

```rust
#[async_trait]
pub trait Flows: Send + Sync {
    async fn get(&self, flow_ip: &IpAddr) -> Option<FlowInfo>;
    async fn all(&self) -> Vec<FlowInfo>;
}
```
___

# Macros [:link:](https://doc.rust-lang.org/reference/macros.html)

| Macro Type      | Syntax                      | Purpose                               |
| --------------- | --------------------------- | ------------------------------------- |
| **Declarative** | `macro_rules!`              | Pattern matching, <br>code generation |
| **Procedural**  | `#[proc_macro...]`          | Advanced code transformations         |
| Derive Macros   | `#[derive(...)]`            | Auto-implement common traits          |
| Builtin Macros  | `assert!`, `println!`, etc. | Core functionality, debugging         |
___

# Declarative Macros

```rust
#[macro_export]
macro_rules! vec {
    ( $( $x:expr ),* ) => {
        {
            let mut temp_vec = Vec::new();
            $(
                temp_vec.push($x);
            )*
            temp_vec
        }
    };
}
```

___

# Procedural Macros

| Macro Type     | Syntax                              | Purpose                                   |
| -------------- | ----------------------------------- | ----------------------------------------- |
| Custom Derive  | <code>#[proc_macro_derive(X)]<code> | Derive attr to structs & enums            |
| Attribute-like | <code>#[proc_macro_attribute]<code> | Like derive,<br>new attribute                |
| Function-like  | `#[proc_macro]`                     | Like declarative, more flexible & complex |

___

# Procedural Macros

Custom Derive:
```rust
#[derive(X)]
struct Pancakes;
```
Attribute-like: <!-- route is the new attribute --->
```rust
#[route(GET, "/")]
fn index() { ... }
```
Function-like:
```rust
let sql = sql!(SELECT * FROM posts WHERE id=1);
```

___

![bg](./images/rusty_background.png)

# Tooling:hammer:

___

# cargo:truck:
= Package manager

| Commands |           |         |
| -------- | --------- | ------- |
| new      | init      | clean   |
| check    | test      | bench   |
| build    | run       | fmt     |
| doc      | search    | publish |
| install  | uninstall | update  |

___


## Identical dir structure

```
❯ cargo build Test
...

❯ tree Test
Test
├── Cargo.toml
├── src
│   ├── main.rs
│   └── lib.rs
├── tests|bench|examples
│   └── util.rs
└── target
    ├── debug
    │   └── build
    └── doc
...
```
___


## Documentation & testing

Generate full webpage of doc: `cargo doc`

Launches doc in browser `cargo doc --open`

Run all tests & doc examples: `cargo test`

___

## Toolchain file


```toml
[toolchain]
channel = "1.74.1"
components = ["cargo", "clippy", "rustfmt", "llvm-tools-preview"]
targets = ["armv7-unknown-linux-gnueabihf", "aarch64-unknown-linux-gnu"]
```
[:link: rustup book](https://rust-lang.github.io/rustup/overrides.html#the-toolchain-file)

```sh
❯ rustup target list --installed
aarch64-unknown-linux-gnu
armv7-unknown-linux-gnueabihf
x86_64-unknown-linux-gnu
```

___

<style scoped> section { text-align: center; }</style>

## Rich IDE support

![width:800](./drawings/IDE_features.drawio.svg)

___

## [cxx](https://cxx.rs/extern-c++.html): Modern C++ integration

```toml
# In Cargo.toml
[dependencies]
cxx = "1.0"

[build-dependencies]
cxx-build = "1.0"
```
```rust
mod conan_cargo_build;

fn main() {
    conan_cargo_build::set_instructions_for_conan_libs();
    cxx_build::bridge("src/main.rs")
        .flag_if_supported("stdc++17")
        .includes(conan_cargo_build::_INCLUDE_PATHS)
        .compile("project_name");
}
```

___

## [cxx](https://cxx.rs/extern-c++.html): Modern C++ integration

```rust
#[cxx::bridge]
mod factory {
    unsafe extern "C++" {
        include!("factory.h");
        ...
    }
}
```
```rust
#[cxx::bridge]
mod ffi {
    extern "Rust" {
        ...
    }
}
```
___

## Serialization

```toml
# In Cargo.toml
[dependencies]
serde = {version = "1.0.210", features = ["derive"]}
serde_json = { version = "1.0.128" }
```
```rust
use serde::Serialize;

#[derive(Serialize)]
struct Bla(i32);

fn main() {
    println!("{}", serde_json::to_string(&Bla(2)).unwrap() );
}
```
___

## Mocking with `mockall`

```toml
# In Cargo.toml
[dev-dependencies]
mockall = "0.13"
```
```rust
#[automock]
trait MyTrait {
    fn foo(&self) -> u32;
    fn bar(&self, x: u32, y: u32) -> u32;
}

let mut mock = MockMyTrait::new();
mock.expect_foo()
    .return_const(42u32);
mock.expect_bar()
    .returning(|x, y| x + y);
```

___

## Cucumber BDD tests 🥒
```gherkin
    Scenario: One stream gets published

    When an AES67 flow with IP 239.1.0.1 is started
    And language NL for AES67 IP 239.1.0.1 gets configured
    And a meeting becomes active
    Then the stream is published
```
___

## Cucumber BDD tests 🥒


```rust
    #[given(expr = "a tokbox session is active")]
    async fn session_ongoing(w: &mut World) {
        let session_info = tokbox_helper::create_session().await;
        REDIS.publish_session_info(session_info.clone());

        w.session = Some(session_info);
    }
```
```rust
#[then(regex = "the stream is (not |un)?published")]
async fn stream_published(w: &mut World, published: String) {
	let published = published.is_empty();
	w.expected_streams = published as u8;
	check_stream_published(w, published).await
}
```

___


# Projects & Examples

Cmd line tools: starship, ripgrep, fastmod etc
[:link: Warp](https://www.warp.dev/)
[:link: Dioxus projects](https://dioxuslabs.com/awesome#made-with-dioxus)
[:link: eGui](https://www.egui.rs/#demo)
[:link: Bevy game engine](https://bevyengine.org/examples/#3d-rendering)

Find your tool: "www.AreWe...Yet.com"

___

# More resources

[:link: Official docs](https://www.rust-lang.org/learn)
[:link: rust-by-example](https://doc.rust-lang.org/rust-by-example/)
[:link: rustlings exercises](https://github.com/jaques-sam/rustlings)
[:link: cheats.rs](https://cheats.rs/)
[:link: awesome-rust](https://github.com/rust-unofficial/awesome-rust)
[:link: Rust Github](https://github.com/rust-lang)

___


# Conclusion
![bg](./images/rusty_background.png)
___

# Recap

- Rust Pillars
- Media
- Basics
- Advanced
- Tooling
- Examples

___

# Uncovered

- Advanced language features:
  - Unsafe Rust
  - Advanced Traits & Types
  - Advanced Functions and Closures
  - Conversions
- Many cargo features
  - workspaces
  - publishing
  - build.rs

___

# So why rust?

- Rust brings back memory management in nicer way
- Embrace correctness in a post-modern world
- Stop debate over the value of a type system
- We want fast & robust software!
- Let's crush web app js world with wasm

___

<!-- footer: "by SJ" -->
![bg](./images/rusty_background.png)
<style scoped> section { text-align: center; }</style>

# Questions?

![](./images/rust-logo-blk.svg)


**Or go straight to the [book :smile:](https://doc.rust-lang.org/stable/book/)**

___
<style scoped> section { text-align: center; }</style>

![bg](./images/rusty_background.png)

**This presentation was created by Sam Jaques
in markdown using Marp**
[![width:300](./images/marpit.png)](https://marpit.marp.app/)

