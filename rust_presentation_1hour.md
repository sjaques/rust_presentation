---
marp: true
title: Rust
theme: uncover
style: |
  section {
    background-color: #fffaff;
  }
  section.left {
    padding-left: 20%;
    text-align: left;
  }
  h1 {
    font-variant: small-caps;
    line-height: 1;
  }
  h2 {
    line-height: 0.7;
  }
  h3 {
    line-height: 0.5;
  }

mark_down_annotations:
---

![bg left](./images/rust_background.png)

# Rust  <!-- TIME: 30" -->
www.rust-lang.org

_

*by Sam Jaques*

---

# Rust, what?  <!-- TIME: 2' -->

![width:800](./drawings/rust_what.drawio.svg)

[All Rust editions](https://doc.rust-lang.org/edition-guide/index.html)

---
<!-- paginate: true -->
<!-- header: "Rust" -->

# Audience  <!-- TIME: 1' -->
* **PART 1: Rationale -** Anyone interested
    - The why
    - Its principles
  <br>
* **PART 2: In depth -** Mainly for developers
    - Similar C/C++ features: informal
    - The unknowns: deeper dive

---

![bg left](./images/rust_background.png)

# Part I
# Rationale

---
<!-- _class: left -->

![bg](./images/rusty_background.png)

# Rust's pilars

## :one: Reliability
## :two: Performance
## :three: Productivity

---

# :one: Reliability  <!-- TIME: 45" -->

## Memory safety
- No dangling pointers
- No memory corruptions
- No resource leaks

**Guaranteed** @ compile time!
:arrow_right: No memory testing (like Valgrind)

No invalid indexes :arrow_right: panic otherwise

---

# Bugs in ClickShare  <!-- TIME: 30" -->

- [**262 iTracks** with search "segmentation fault" OR "segfault" OR "seg fault"](https://itrack.barco.com/browse/CS0042-2239?jql=project%20in%20(CS0042%2C%20CS0040%2C%20CS0048%2C%20CS0100)%20AND%20text%20~%20%22%5C%22segmentation%20fault%5C%22%20OR%20%5C%22segfault%5C%22%20OR%20%5C%22seg%20fault%5C%22%22)
- [**152 iTracks** with search "memory leak"](https://itrack.barco.com/browse/CS0042-8665?jql=project%20in%20(CS0042%2C%20CS0040%2C%20CS0048%2C%20CS0100)%20AND%20text%20~%20%22memory%20leak%22)

  :arrow_right: hours spend? 🤔

---

# :two: Performance  <!-- TIME: 30" -->

## Blazingly fast :bullettrain_side:  <!-- C & C++ alike -->
## Memory-efficient :pinching_hand:
## No garbage (collector) :deciduous_tree:

Benchmarks:
[Rust vs C++](https://benchmarksgame-team.pages.debian.net/benchmarksgame/fastest/rust-gpp.html), [Rust vs C](https://benchmarksgame-team.pages.debian.net/benchmarksgame/fastest/rust.html)
<!-- But what they don't mention often is that, if you compare the Rust with the C or C++ code, that there is a major difference in readability and complexity. If you want to have the same performance and memory efficiency of a Rust program, you'll have to squeeze out C++ language features to the maximum.$$ -->

---

# :three: Productivity  <!-- TIME: 30" -->
![width:800](./drawings/productivity.drawio.svg)

---

<!-- paginate: true -->
<!-- header: "rust" -->

# Media
![bg](./images/rusty_background.png)

---
<!-- header: "" -->

[Why Discord is switching from Go to Rust](https://blog.discord.com/why-discord-is-switching-from-go-to-rust-a190bbca2b1f)

> *Even with just basic optimization, Rust was able to outperform the hyper hand-tuned Go version. This is a huge testament to how easy it is to write efficient programs with Rust compared to the deep dive we had to do with Go.*

![width:800](https://miro.medium.com/max/3000/1*-q1B4t622mnxoV8kvT9RwA.png)

---

[Linus Torvalds response in IT Wire](https://itwire.com/open-source/rust-support-in-linux-may-be-possible-by-5-14-release-torvalds.html):

> *Rust support was 'not there yet', adding that things were 'getting to the point where maybe it might be mergeable for 5.14 or something like that...'*

> *...at least it's in a 'this kind of works, there's an example, we can build on it'.*

---

[Google is now writing low-level Android code in Rust](https://arstechnica.com/gadgets/2021/04/google-is-now-writing-low-level-android-code-in-rust/)

> *memory safety bugs continue to be a top contributor of stability issues, and consistently represent ~70% of Android’s high severity security vulnerabilities.*

> *Most of our memory bugs occur in new or recently modified code. Rust will be used for new components, when necessary, which should help reduce any new memory bugs Google could introduce.*

---

[Most loved language since 2015 YoY](https://survey.stackoverflow.co/2024/technology#admired-and-desired)

> *Rust continues to be the most-admired programming language with an 83% score in 2024.*

![most_beloved width:1000](./images/stack-overflow-most-loved-programming-languages.png)

---

<!-- header: "" -->

## AWS Open Source Blog
[Innovating with Rust](https://aws.amazon.com/blogs/opensource/innovating-with-rust/)

> *"The Rust language has quickly become critical to building infrastructure at scale at Amazon Web Services (AWS)"*

[Sustainability with Rust](https://aws.amazon.com/blogs/opensource/sustainability-with-rust/)

> *"We started hiring Rust maintainers and contributors, partnered with Google, Huawei, Microsoft and Mozilla to create the Rust Foundation with a mission to support Rust. AWS is investing in the sustainability of Rust, a language we believe should be used to build sustainable and secure solutions. "*

---

# Downsides  <!-- TIME: 1' -->

- Steep learning curve <!-- concepts from system programming like C, with higher level language like Scala & Haskell -->
- Complexity
  - multiple _pointer_ types (`&`,`Rc`,`Arc`,`Box`,`Cell`...)
  - two different string types (`&str`and`String`)
  - generics
  - _lifetimes_ (feature) can be confusing

----
<!-- header: "rust" -->

![bg](./images/rusty_background.png)

# Rust's "secret sauce" :flying_saucer:

---

# Enforced Rules  <!-- TIME: 30" -->

**If you allow something, people use it**

_(18 ways of initializing a variable in C++11 :open_mouth:)_
<!-- this doesn't mean they are all good. They can lead to inconsistency too. -->

<br>


**If you don't enforce, people make mistakes**
_even the best :horse:_

---

# Enforcements  <!-- TIME: 30" -->

- Static typing
- Impossible to use null values
- Transfer of ownership
- Undefined behavior :arrow_right: panic!
- ...

---

## Null values cannot be used <!-- TIME: 1' -->
Tony Hoare (inventor of _null_):

  > _Null References: The Billion Dollar Mistake_

<br>

:arrow_right: Value _present_ or _absent_:
<!-- try to use a null value as a not-null value, you’ll get an error of some kind -->

```rust
enum Option<T> {
    None,
    Some(T),
}
```

---

# Transfer of ownership  <!-- TIME: 30" -->

<br>

![width:600](./drawings/ownership.drawio.svg)

- Implicitely enforced
- Object lifetime checks
- Inexpensive by design


<!-- includes forced *"move semantics" -->

---

<!-- TIME: 30" -->
# _The borrow checks_ [:link:](https://rustc-dev-guide.rust-lang.org/borrow_check.html)

_**Many immutable borrows as you like (`&`)
**OR**
A single mutable borrow  (`&mut`)**_

---

# Borrowing example  <!-- TIME: 1' -->

```rust
fn hold_my_vec<T>(_: Vec<T>) {
}

fn main() {
    let v = vec![1, 2, 3, 5, 7, 11, 13, 17];
    hold_my_vec(v);  // culprit
    let element = v.get(3);

    println!("Element '{:?}' taken from vector", element);
}
```

---

Compiler prints understandable error

```rust
error[E0382]: borrow of moved value: `v`
--> src/main.rs:6:19
    |
  5 | hold_my_vec(v);
    |             - value moved here
  6 | let element = v.get(3);
    |               ^ value borrowed here after move
```

---

Solution: borrow the vector as **immutable** reference  <!-- TIME: 1' -->

```rust
fn hold_my_vec<T>(_: &Vec<T>) {}

fn main() {
    let v = vec![2, 3, 5, 7, 11, 13, 17];
    hold_my_vec(&v);
    let element = v.get(3);

    println!("Element '{:?}' taken from vector", element);
}
```

---

# Lifetime constraints  <!-- TIME: 1' -->

```rust
{
    let x: &Vec<i32>;  // uninitialized reference
    {
        let y = Vec::new();

        x = &y;

    }
    println!("x's length is {}", x.len());
}
```

---

# Lifetime constraints  <!-- TIME: 1' -->

```rust
{
    let x: &Vec<i32>;  // uninitialized reference
    {
        let y = Vec::new();  //--+
//                               | y's lifetime
        x = &y;  //--------------|--------------+
//                               |              |
    }  // <----------------------+              | x's lifetime
    println!("x's length is {}", x.len());//    |
}  // <-----------------------------------------+

error: `y` does not live long enough
```

---

# Lifetime annotations  <!-- TIME: 1' -->

- Compiler cannot always determine lifetimes
- One of the harder features
- Getting better each release
- Following example won't compile:
```rust
fn longest<'a>(x: &'a str, y: &'a str) -> &'a str {
    if x < y {
        x
    } else {
        y
    }
}
```

---

# Fearless Concurrency  <!-- TIME: 30" -->

> _Do not communicate by sharing memory;
> instead, share memory by communicating._
<br>


Bowering rules :arrow_right: only 1 owner :heart:

---

# Advantages over C/C++?

![width:800](./drawings/advantages_over_C.drawio.svg)
<!-- - match expressions
     - if/else for const init -->

---
<!-- header: "rust" -->

# Unsafe arithmetics [:link:](https://godbolt.org/z/9b9P6KGfx)

```cpp
// C++
auto square(std::int32_t num) {
    return num * num;
}
```
`46341 * 46341`:arrow_right: UB!: overflow :exclamation:
`-46341 * -46341`:arrow_right: UB!: overflow :exclamation:

<br>

**Range: `[-46340, 46340]`
== `[0xFFFF4AFC, 0xB504]` :-1:**
<!-- So the range is depending on the arithmetical operation! This is nuts! -->

<!-- paginate: true -->
---

# Safe arithmetics [:link:](https://rust.godbolt.org/z/GhenxYbcb)

```rust
// rust
pub fn square(num: i32) -> i32 {
    num * num;
}
```

UB? :arrow_right: literally impossible

<br>

**Range is `[-2147483648, 2147483647]`
== `[0x80000000, 0x7FFFFFFF]`**  :+1:

---
<!-- header: "" -->

# C++ Compile error hints:weary:

    ‘from std::_Rb_tree<_Key, _Val, _KeyOfValue, _Compare, _Alloc>::iterator
    std::_Rb_tee<_Key, _Val, _KeyOfValue, _Compare, _Alloc>::
    _M_emplace_hint_unique(std::_Rb_tree<_Key, _Val, _KeyOfValue, _Compare,
    _Alloc>::const_iterator, _Args&& ...) [with _Args =
    {const std::piecewise_construct_t&, std::tuple<const SomeType&>,
    std::tuple<>}; _Key = SomeType; _Val = std::pair<const SomeType, SomeData>;
    _KeyOfValue = std::_Select1st<std::pair<const SomeType, SomeData> >;
    _Compare = std::less<SomeType>; _Alloc =
    std::allocator<std::pair<const SomeType, SomeData> >;
    std::_Rb_tree<_Key, _Val, _KeyOfValue, _Compare, _Alloc>::iterator =
    std::_Rb_tree<SomeType, std::pair<const SomeType, SomeData>,
    std::_Select1st<std::pair<const SomeType, SomeData> >,std::less<SomeType>,
    std::allocator<std::pair<const SomeType, SomeData> > >::iterator;
    std::_Rb_tree<_Key, _Val, _KeyOfValue, _Compare, _Alloc>::const_iterator =
    std::_Rb_tree<SomeType, std::pair<const SomeType, SomeData>,
    std::_Select1st<std::pair<const SomeType, SomeData> >, std::less<SomeType>,
    std::allocator<std::pair<const SomeType, SomeData> > >::const_iterator]’

<!-- Only 2 types created: `SomeType` & `SomeData` rest is std -->

<!-- paginate: false -->
---

![bg](./images/rusty_background.png)

# Tooling:hammer:

<!-- paginate: true -->
---

# Compilation  <!-- TIME: 30" -->

- 1 compiler: `rustc`
- Supported platforms [:link:](https://doc.rust-lang.org/stable/rustc/platform-support.html)
- Cross platform by default
`❯ rustc main.rs --target=aarch64-wrs-vxworks`

---

# Cargo:truck: [:link:](https://doc.rust-lang.org/cargo/) <!-- TIME: 1' -->
= Package manager

| Commands |           |         |
| -------- | --------- | ------- |
| new      | init      | clean   |
| check    | test      | bench   |
| build    | run       | fmt     |
| doc      | search    | publish |
| install  | uninstall | update  |

---
<!-- header: "" -->

# Identical dir structure  <!-- TIME: 20" -->

```
❯ cargo build home
...

❯ tree home
home
├── Cargo.toml
├── src
│   ├── main.rs
│   └── lib.rs
├── tests
│   └── util.rs
└── target
    ├── debug
    │   └── build
    └── doc
...
```
---
<!-- header: "rust" -->

# Documentation & testing  <!-- TIME: 30" -->
<br>

Generate uniform webpage of doc: `cargo doc`

Launches doc in browser `cargo doc --open`

Run all tests (incl. doc examples): `cargo test`

---
<!-- header: "" -->

# Rich IDE support  <!-- TIME: 30" -->

![width:600](./drawings/IDE_features.drawio.svg)
![width:700](./images/IDE_support.png)

---
<!-- header: "rust" -->

# WebAssembly [wasm] [:link:](https://rustwasm.github.io/docs/book/) <!-- TIME: 30" -->
:arrow_right: Fast stack-based virtual machine
[made with webassembly](https://madewithwebassembly.com/)

<br>
<br>
<br>
<br>

![bg invert width:150](./images/gears.svg?text=A)
<!-- low-level control coupled with high-level ergonomics -->
![bg invert width:150](./images/microscope.svg?text=C)
<!-- small code size means faster page loads, no bloat -->
![bg invert width:150](./images/luggage.svg?text=B)
<!-- A lively ecosystem of libraries to help you hit the ground running. -->
<!-- Expressive, zero-cost abstractions. -->
![width:1100](./drawings/webassembly.drawio.svg)

---
<!-- header: "rust" -->
<!-- _class: left -->

![bg](./images/rusty_background.png)

# Why Rust<br>for developers

➖ New framework (integration)
➖ Steep learning curve

➕ Splendid memory managment
➕ Embrace correctness in post-modern world
➕ Let's crush web app js world with wasm
➕ Resemblance of C & C++ mindsets

---
![bg](./images/rusty_background.png)
<!-- _class: left -->

# Why Rust for stakeholders

➖ Lack of experienced engineers

➕ Industry proven quality
➕ Better runtime performance
➕ Low energy usage
➕ Shorter validation cycle
➕ More robust software!

---

<!-- header: "rust" -->

![bg](./images/rusty_background.png)

# Questions?  <!-- TIME: 5' -->

---

![bg left](./images/rust_background.png)

# Part II
# In Depth

---
<!-- paginate: true -->
<!-- header: "rust" -->
![bg](./images/rusty_background.png)

# Basics

___

# Primitive types [:link:](https://doc.rust-lang.org/std/index.html#primitives)  <!-- TIME: 30" -->

- `bool`,`char`,`usize`,`isize`,`f32`,`f64`
- `i8`,`u8`,`i16`,`u16`,... ,`i128`
- fixed-size array `[T; N]`[:link:](https://doc.rust-lang.org/std/primitive.array.html)
- function pointer `fn`
- reference`&` and `mut&`
- slice `[T]` and string slice `str`
- tuple`(T, U, ..)`

---
<!-- header: "" -->

# Control flow

- `if`/`else` including:
  ```rust
  let number = if condition { 5 } else { 6 };
  ```

- `loop {...}` ⇔ `while true {...}`  <!-- including break & continue -->
- ranged loops:
  ```rust
  for number in (1..4).rev() {  // upper limit excluded
      println!("{}!", number);
  }
  ```

---

# Pattern matching

<!-- Match expression is exhaustive: need all patterns! -->

```rust
match number {
    1 => println!("one"),
    2 | 3 => println!("two or three"),
    4..=9 => println!("within range"),  // upper limit included
    x if x > 10 => println!("more than ten: {}", x),
    _ => println!("default Case")  // Match expressions are exhaustive
}
```

---

<!-- header: "" -->

# Structs

```rust
struct User {  // Definition
    username: String,
    active: bool,
}

impl User {  // Implementation
    fn is_active(&self) -> bool {
        self.active
    }
}

fn main() {
    let samja = User {  // Instantiation
        username: String::from("samja"),
        active: true,
    };
    let isActive = samja.isActive();
}
```

---
<!-- header: "rust" -->

# Enums  <!-- TIME: 1' -->

```rust
enum Message {
    Quit,  // first variant
    Move{x: i32, y: i32},  // struct
    Write(String),  // tuple struct: fields have no name
    ChangeColor(i32, i32, i32),  // tuple struct
}  // biggest variant size => Message size

impl Message {
    fn call(&self) {
        // tadaaa
    }
}

let m = Message::Write(String::from("hello"));
m.call();
```
---
<!-- header: "" -->

# Slice type `&[T]`  <!-- TIME: 30" -->

``` rust
let s = String::from("Hello world");
let hello = &s[0..5];
let world = &s[6..11];
```
```rust
fn first_word(s: &str) -> &str {
    for (i, item) in s.char_indices() {
        if item == ' ' {
            return &s[0..i];
        }
    }

    &s[..]
}
```

---

# Slice type `&[T]`

![width:500](./drawings/string_slice.drawio.svg)

---
<!-- header: "rust" -->

# Constants

```rust
// Globals: declared outside all scopes
const THRESHOLD: i32 = 10;
static LANGUAGE: &str = "Rust ❤️";  // `'static` lifetime

fn main() {
    let n = 16;

    println!("This is {}", LANGUAGE);
    println!("The threshold is {}", THRESHOLD);
    let saturated = n > THRESHOLD;

    THRESHOLD = 5;  // Error! Cannot modify a `const`.
    LANGUAGE = "C++";  // Error! Cannot reassign static
}
```

---

# Traits
⇔ interface(s)

- :heavy_check_mark: **Dynamic dispatch** :arrow_right: trait objects
:x: inheritance
- :heavy_check_mark: **Static dispatch** :arrow_right: with generics

```rust
trait Animal {
    fn noise(&self) -> String { String::from("") }
    fn has_tail(&self) -> bool;
}
```
---
<!-- header: "" -->
# Traits
## Implemented on struct

```rust
struct Dog { name: String }

impl Animal for Dog {
    fn noise(&self) -> String {
        String::from("woof!")
    }
}

impl Dog {
    fn new(name: String) -> Self {  // equal to new<T: Dog>
        Dog { name }  // omit param name: param name == arg name
    }
    fn fetch(&self) { }
}
```
---

# Traits
## Dynamic dispatch

```rust
struct Zoo {
    animals: Vec<Box<dyn Animal>>,  // Box has a fixed size
}

impl Zoo {
    fn make_some_noise(&self) {
        for animal in self.animals.iter() {
            println!("{}", animal.noise());  // vtable created
        }
    }
}
```
---
<!-- header: "rust" -->
# Special traits
## Deref coercion (= call chain)

```rust
fn hello(name: &str) {
    print!("Hello {}!", name);
}

fn main() {
    let name = Box::new(String::from("Sam"));
    hello(&name);  // &Box<String> -> &String -> &str

    hello(&(*name)[..]);  // without Deref coercion
}
```

---

<!-- header: "rust" -->

![bg](images/rusty_background.png)
![width:800](images/rust_vs_c++.jpg)

# More C/C++ Comparisons

---

# Semantic similarities

- Similar to modern C++
- moves are forced (implicitly)
- `const` (immutable) by default, otherwise add`mut`
- safer`&` and explicit when passing: `hold_my_vec(&v)` <!-- in C++ no `&` symbol is present when passing by reference; references can never be DEADBEAF -->
- `&self` ⇔ C++`this`pointer

---
<!-- header: "" -->

# Other known features
- **Aliasing**: `use house::kitchen` (iso `using`)
- **Macros**: conceptionally the same, harder to misuse
  _examples_: `vec!`,`println!`,`panic!`,`debug_assert!`

- **Generics`<T>`**: similar to templates, used more often
- **Closures**: similar to lambdas
  _examples_:
  ```rust
  fn  add_one_v1   (x: u32) -> u32 { x + 1 }
  let add_one_v2 = |x: u32| -> u32 { x + 1 };
  let add_one_v3 = |x|             { x + 1 };
  let add_one_v4 = |x|               x + 1  ;
  ```
<!-- paginate: false -->

---
<!-- header: "rust" -->

# Error handling

1. Recoverable errors: `Result<T, E>` and `?`
2. Unrecoverable errors: `panic!`like C`assert`

:x: No "exceptions" concept

<!-- paginate: true -->
---
<!-- header: "" -->

## Error handling in ⩾C++17

```cpp
#include <string>
#include <optional>
#include <fstream>

std::optional<std::string> read_line_from_file() {
    std::ifstream file("my_file.txt");
    if (!file) {
        return {};
    }

    std::string line;
    file >> line;
    if (!file.good()) {
        return {};
    }
    return line;
}
```

---

## Error handling in Rust (long)

```rust
use std::fs::File;
use std::io::{self, Read};

fn read_line_from_file() -> Result<String, io::Error> {
    let f = File::open("my_file.txt");
    let mut f = match f {
        Ok(file) => file,
        Err(e) => return Err(e),
    };

    let mut s = String::new();
    match f.read_to_string(&mut s) {
        Ok(_) => Ok(s),
        Err(e) => Err(e),
    }
}
```

---

## Error handling in Rust (short)

```rust
use std::fs::File;
use std::io::{self, Read};

fn read_line_from_file() -> Result<String, io::Error> {
    let mut file = File::open("my_file.txt")?;  //--> propagate error
    let mut line = String::new();
    file.read_to_string(&mut line)?;
    Ok(line)
}
```
Or simply :smile:
```rust
let content = fs::read_to_string("my_file.txt")?;
```

---

# std types in prelude [:link:](https://doc.rust-lang.org/std/prelude/index.html)
<!-- prelude: list of things that Rust automatically imports into every Rust program.
 It’s kept as small as possible, focused particularly traits, used in almost every program.-->

| Rust                  | ~C++ equivalent   |
| :-------------------- | :---------------- |
| `std::option::Option` | `std::optional`   |
| `std::result::Result` | -                 |
| `std::vec::Vec`       | `std::vector`     |
| `std::String`         | `std::string`     |
| `std::boxed::Box`     | `std::unique_ptr` |
...

<!-- Much more in prelude like
- std::marker::{Copy, Send, Sized, Sync, Unpin}
- std::ops::{Drop, Fn, FnMut, FnOnce}
- std::clone::Clone
- std::cmp::{PartialEq, PartialOrd, Eq, Ord}
-->

---

# Other smart pointers
## & containers

- `std::rc::Rc<T>` <br>(=<ins>**R**</ins>eference <ins>**c**</ins>ounting)
- `std::sync::Arc<T>`(=<ins>**A**</ins>tomic)
- `Cell<T>`/`RefCell<T>`
- `std::collections::HashMap/Set`
- `std::collections::LinkedList`

C++`std::shared_ptr` equivalent?:`Arc<Mutex<T>>` or `Arc<RwLock<T>>` or `Arc<UnsafeCell<T>>`

<!--  Rc<T> is a safe default, because the compiler will catch any attempt to send an Rc<T> between threads-->

---

## (Ref)Cell

![width:500](./drawings/cell_within_immutable_struct.drawio.svg)

- :exclamation: Borrowing checks @ runtime!
**Hazard:** exclusive access needed when modifying

- `Cell<Rc<T>>`&`Cell<Arc<T>>` :arrow_right: mutable shared ownership

---

## `Cell<T>` vs `mut &T`

| Runtime check                                                                                                                             |                                                                                                        Compile time check |
| :---------------------------------------------------------------------------------------------------------------------------------------- | ------------------------------------------------------------------------------------------------------------------------: |
| <pre><code>let x = Cell::new(1);<br>let y = &x;<br>let z = &x;<br>x.set(2);<br>y.set(3);<br>z.set(4);<br>println!("{}", *x);</code></pre> | <pre><code>let mut x = 1;<br>let y = &mut x;<br>let z = &mut x;<br>x = 2;<br>*y = 3;<br>*z = 4;<br>println!("{}", x);<br> |
| </code>                                                                                                                                   |

- Same behavior
- Same runtime cost: none!:thumbsup:

---

| Pointer type | Features [:link:](https://manishearth.github.io/blog/2015/05/27/wrapper-types-in-rust-choosing-your-guarantees/) |
| :----------- | :--------------------------------------------------------------------------------------------------------------- |
| `&T`         | Single ownership, constant data                                                                                  |
| `&mut T`     | Single ownership, interior mutability                                                                            |
| `Box<T>`     | Single ownership                                                                                                 |
| `Rc<T>`      | Multiple ownership                                                                                               |
| `Arc<T>`     | Multiple ownership, threadsafe                                                                                   |
| `Cell<T>`    | Interior mutability for`copy`types                                                                               |
| or `RefCell<T>` | Interior mutability                                                                                              |
| `Mutex<T>`   | Block threads, RAII guard                                                                                        |
| or `RwLock<T>`  | =mutex, 1 lock for multiple readers                                                                              |
<!-- Interior mutablity: In other words, they contain data which can be manipulated even if the type cannot be obtained in a mutable form -->

---
<!-- header: "rust" -->

# Pointer types

![width:800](./drawings/pointer_types_more.drawio.svg)

---

# Iterations
## In C++(17)
```cpp
v.erase(std::remove_if(std::begin(v), std::end(v), is_odd), std::end(v));
```
:arrow_right: contains 2 loops

## In Rust
```rust
let v: Vec<_> = v.iter().filter(|&x| !is_odd(x)).collect();
```
:arrow_right: `collect`consumes all`iter`s, once
<!-- `collect` can also convert a container to another type-->

---
<!-- header: "rust" -->

## Iterator performance

- Zero-cost abstractions: no runtime overhead
- Compiles identical to assembly without loop
- Loop vs iterator:
    1. The Adventures of Sherlock Holmes :arrow_right: string
    2. Count the word _the_
```
test bench_search_for  ... bench:  19,620,300 ns/iter (+/- 915,700)
test bench_search_iter ... bench:  19,234,900 ns/iter (+/- 657,200)
```

---

![bg](./images/rusty_background.png)
# Fearless Concurrency

---

# Message passing: example

```rust
use std::sync::mpsc;
use std::thread;

fn main() {
    let (tx, rx) = mpsc::channel();

    thread::spawn(move || {  // tx moves to closure
        let val = String::from("hi");
        tx.send(val).unwrap();
    });

    let received = rx.recv().unwrap();
    println!("Got: {}", received);
}
```

---

# Shared state

- Immutuable data :arrow_right:`Arc<RefCell<T>>`
- Mutable data :arrow_right: `RwLock<T>`or`std::Mutex<T>`

<br>

Locking dicipline enforced:

**lock-protected data is only accessible
when holding the lock**  :heart:

---

## Dead locks still possible :anguished:
_when using a `Mutex`/`RwLock`_

```rust
use std::sync::Mutex;

fn main() {
    let data = Mutex::new(0);
    let _d1 = data.lock();
    let _d2 = data.lock(); // cannot lock, since _d1 is still active

    println!("is not printed");
}
```
---

![bg](./images/rusty_background.png)

# Libraries & <br>C/C++ integration


---
<!-- header: "" -->

## [cxx](https://cxx.rs/extern-c++.html): Modern C++ integration

```toml
# In Cargo.toml
[dependencies]
cxx = "1.0"

[build-dependencies]
cxx-build = "1.0"
```
```rust
mod conan_cargo_build;

fn main() {
    conan_cargo_build::set_instructions_for_conan_libs();
    cxx_build::bridge("src/main.rs")
        .flag_if_supported("stdc++17")
        .includes(conan_cargo_build::_INCLUDE_PATHS)
        .compile("project_name");
}
```

---

## [cxx](https://cxx.rs/extern-c++.html): Modern C++ integration

```rust
#[cxx::bridge]
mod factory {
    unsafe extern "C++" {
        include!("factory.h");
        ...
    }
}
```
```rust
#[cxx::bridge]
mod ffi {
    extern "Rust" {
        ...
    }
}
```

---

## Mocking with `mockall`

```toml
# In Cargo.toml
[dependencies]
mockall = "0.11"
```
```rust
#[automock]
trait MyTrait {
    fn foo(&self) -> u32;
    fn bar(&self, x: u32, y: u32) -> u32;
}

let mut mock = MockMyTrait::new();
mock.expect_foo()
    .return_const(42u32);
mock.expect_bar()
    .returning(|x, y| x + y);
```

---

<!-- footer: "by samja" -->
![bg](./images/rusty_background.png)

# Questions?

![](./images/rust-logo-blk.svg)


Or go straight to the book :smile:
https://doc.rust-lang.org/stable/book/

<!-- paginate: false -->
---

![bg](./images/rusty_background.png)

This presentation was created by Sam Jaques
in markdown using Marp [![width:200](./images/marpit.png)](https://marpit.marp.app/)

